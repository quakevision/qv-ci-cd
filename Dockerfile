# docker build -t angular-app .
# docker run --rm -it -p 4200:80 angular-app
FROM nginx:alpine

WORKDIR /app
COPY . /app

# entrypoint.sh
RUN \
    mv ./default.conf /etc/nginx/conf.d/default.conf 
    #&& \
    #rm ./config/config.js && \
    #mv ./config/config.prod.js ./config/config.js -f

EXPOSE 80

# Debug using: ENTRYPOINT ["sh"]
ENTRYPOINT ["nginx", "-g", "daemon off;"]